/* global $ */

$(document).ready(function() {
    $('#search_button').on('click', function(){
        $('.hideon').each(function() {
           $(this).toggleClass('hiden'); 
        });
        $('#search_input').toggleClass('hiden');
    });
    $(window).scroll(function() {
        if($(this).scrollTop() != 0) {
            $('#up').fadeIn();
            $(".visibility-eye").css("top","75px");
        } else {
            $('#up').fadeOut();
            $(".visibility-eye").css("top","225px");
        }
        });
    
    $('#up').click(function() {
        $('body,html').animate({scrollTop:0},800);
    });
            
    $('#vitrina-icon-bottom').hover(function(){
        $(this).transition('pulse');
    }, function(){});
    $('#cart-icon-bottom').hover(function(){
        $(this).transition('pulse');
    }, function(){});
    
    $( ".orange" ).each(function() {
        $( this ).removeClass( "orange" );
        $( this ).addClass( "olive" );
    });
});