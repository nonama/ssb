/* global $ */

// Helper function to redirect on ogin url if ajax view require login
        function check_login_url(xhr) {
            var login_url = '{% url "accounts:login" %}' + "?next={% url 'goods:goods_modal' good.pk %}";
            if (xhr.responseURL.indexOf('login') != -1 ) {
                // Check if response doesn't contain redirect to login. If it contains - redirect to login window
                window.location = login_url;
            }
        }
        // Get CSRF cookie protection
        var csrftoken = $.cookie('csrftoken');
        function csrfSafeMethod (method) {
            // These HTTP methods do not require CSRF protection
            return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
        }
        $.ajaxSetup({
            beforeSend: function (xhr, settings) {
                if(!csrfSafeMethod(settings.type) && !this.crossDomain) {
                    // Set X-CSRFToken HTTP Header
                    xhr.setRequestHeader("X-CSRFToken", csrftoken);
                }
            }
        });

$(document).ready(function() {
    
    

if ($('#modal-like-button').data('action') == 'add') {
                $('#modal-like-button').find( "i" ).addClass('red');
            } else {
                $('#modal-like-button').find( "i" ).removeClass('red');
            }
            // Send AJAX to remove/add to users likes
            $('#modal-like-button').click(function (e) {
                e.preventDefault();
                console.log('like');
                var previous_action = $('#modal-like-button').data('action');
                $.post('/goods/add-user-like/', {
                    pk: $(this).data('id'),
                    action: $(this).data('action')
                },
                
                function (data, status, xhr) {
                    //check_login_url(xhr);
                    if (data['status'] == 'ok') {
                        
                        $('#modal-like-button').data('action', previous_action == 'add' ? 'remove' : 'add');
                        if (previous_action == 'add') {
                            $('#modal-like-button').find( "i" ).addClass('red');
                            $('#modal-likes-block').append('<br>{% trans "You like this product" %}');
                        } else {
                            $('#modal-like-button').find( "i" ).removeClass('red');

                        }
                    }
                });
            });

            // Wish button scripsts
            // Check action atribute and set image
            if ($('#modal-wish-button').data('action') == 'add') {
                $('#modal-wish-button').css('background','url({% static "images/goods_modal/wish-icon.png" %}) no-repeat 19px 9px');
            } else {
                $('#modal-wish-button').css('background','url({% static "images/goods_modal/wish-icon-pressed.png" %}) no-repeat 19px 9px');
            }
            // Send AJAX to remove/add to wish list
		    $('#modal-wish-button').click(function(e){
                e.preventDefault();
                $.post('{% url "goods:add_to_wishlist" %}',
                    {
                        pk: $(this).data('id'),
                        action: $(this).data('action')
                    },
                    function (data, status, xhr) {
                        check_login_url(xhr);
                        if (xhr.responseURL.indexOf('login') != -1 ) {
                            // Check if response doesn't contain redirect to login. If it contains - redirect to login window#}
                            window.location = '{% url "accounts:login" %}' + "?next={% url 'goods:goods_modal' good.pk %}";
                        }
                        if (data['status'] == 'ok') {
                            var previous_action = $('#modal-wish-button').data('action');
                            $('#modal-wish-button').data('action', previous_action == 'add' ? 'remove' : 'add');
                            if (previous_action == 'add') {
                                $('#modal-wish-button').css('background','url({% static "images/goods_modal/wish-icon-pressed.png" %}) no-repeat 19px 9px');
                            } else {
                                $('#modal-wish-button').css('background','url({% static "images/goods_modal/wish-icon.png" %}) no-repeat 19px 9px');
                            }
                        }
                    }
                );
		    });
		    
});